# installation
## 0. test environment:
    
javascript browser

## 1. Install:

required:

* set up a local/remote postgresql database

* add the database details in file  `app_config.ini` under `psql` column

recommended:

* git

* miniconda

## 2. cd to any folder

copy and run the git clone link on the repo

run `git checkout -b some_name_here`

if not using git: download this repo

## 3. cd to folder with requirements.txt

run `conda create --name environment_name_here python=3.5.2`

activate environment

run `pip install -r requirements.txt` 

* make sure everything is successful, otherwise fix it first


if not using conda: install from `requirements.txt`

## 4. try to run app

run `python app.py`

fill in `app_config.ini`

open local brower and navigate to host:5000 

# development
## 1. debug

use `app.run(debug=True)`

## 2. manage database

* edit and use script `resetdb.py`

* manually login use postgresql to manage