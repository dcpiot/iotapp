import json
from passlib.hash import bcrypt_sha256

from iotapp import applogger, db

class RecordsDatastore(object):

    @staticmethod
    def insert_record(data):
        query = ("""INSERT INTO raw_data
            (device_id, patient_id, record_time, temperature, heart_rate, spo2, ppg)
            SELECT id, patient_id, %s, %s, %s, %s, %s FROM devices WHERE tag = %s;""",
            ('record_time', 'temperature', 'heart_rate', 'spo2', 'ppg', 'device_tag'))
        rowcount = db.execute_query('psql_root', query, data)
        if rowcount['rowcount'] == 0:
            applogger.debug('Data not inserted')
            raise ValueError('Database Constraint Voilated')
        applogger.debug('updating devices date')
        query = ("""UPDATE devices SET (record_time) = (%s) WHERE tag = %s""",
            ('record_time', 'device_tag'))
        rowcount = db.execute_query('psql_root', query, data)
        if rowcount['rowcount'] == 0:
            applogger.debug('Data not inserted')
            raise ValueError('Database Constraint Voilated')

    @staticmethod
    def obtain_ppg(query_choice, query_value):
        if query_choice == 'patient_IC':
            query = (
                """
                SELECT raw_data.ppg FROM raw_data
                INNER JOIN patients ON raw_data.patient_id = patients.id
                WHERE patients.IC = %s ORDER BY raw_data.record_time DESC LIMIT 1
                """, ('patient_IC',))
            data = db.execute_query('psql_root', query, {'patient_IC': query_value})
        if query_choice == 'device_tag':
            query = (
                """
                SELECT raw_data.ppg FROM raw_data
                INNER JOIN devices ON raw_data.device_id = devices.id
                WHERE devices.tag = %s ORDER BY raw_data.record_time DESC LIMIT 1
                """, ('device_tag',))
            data = db.execute_query('psql_root', query, {'device_tag': query_value})
        try:
            data = data['res'][0][0]
            applogger.info('Returning ppg results')
        except (TypeError, IndexError):
            applogger.info('Result is of type %r', type(data))
            return None
        try:
            data = json.loads(data)
        except (json.JSONDecodeError, TypeError):
            applogger.info('Jsondecod error')
            return None
        return data
        

 
    @staticmethod
    def insert_test_record(data):
        query = ("""INSERT INTO raw_data
            (device_id, patient_id, record_time, temperature, heart_rate, spo2)
            SELECT id, patient_id, %s, %s, %s, %s FROM devices WHERE tag = %s;""",
                 ('record_time', 'temperature', 'heart_rate', 'spo2', 'device_id'))
        rowcount = db.execute_query('psql_root', query, data)
        if rowcount['rowcount'] == 0:
            applogger.debug('Data not inserted')
            raise ValueError('Database Constraint Voilated')
        query = ("""UPDATE devices SET (record_time) = (%s) WHERE tag = %s""",
            ('record_time', 'device_id'))
        rowcount = db.execute_query('psql_root', query, data)
        if rowcount['rowcount'] == 0:
            applogger.debug('Data not inserted')
            raise ValueError('Database Constraint Voilated')


class UserDatastore(object):
    """Abstracted user datastore.
    :param user_model: A user model class definition
    :param role_model: A role model class definition
    """

    def __init__(self):
        pass

    @staticmethod
    def find_user(user_id):
        applogger.debug('Loading user with id %s', user_id)
        args = {'user_id': user_id}
        data = db.execute('psql_root', 'load_user', args)['res']
        if not data:
            applogger.debug('User not found')
            return None
        else:
            return data[0]

    @staticmethod
    def validate_password(password, hash_key):
        try:
            validated = bcrypt_sha256.verify(password, hash_key)
        except Exception:
            applogger.error('Invalid password stored')
            return False
        if validated:
            return True
        return False

    @staticmethod
    def authenticate_user(user_id, password):
        data = UserDatastore.find_user(user_id)
        print(data)
        if data is None:
            return None
        if not UserDatastore.validate_password(password, data[1]):
            return None
        return data

    @staticmethod
    def create_user(user):
        """Validates and creates user. Unsuccessful operations will return a status code.
        1: Duplicate user_id
        2: Attempting to create root account
        """
        user_id = getattr(user, 'user_id')
        password = getattr(user, 'password')
        access_level = getattr(user, 'access_level')
        if access_level == 0:
            return 2
        
        hashed_pwd = bcrypt_sha256.encrypt(password)
        args = {'user_id': user_id, 
                'password': hashed_pwd,
                'access_level':access_level}
        try:
            db.execute('psql_root', 'add_new_user', args, tuple(args.keys()))
        except ValueError:
            applogger.warning('Failed to add user with id "%s"', user_id)
            return 1


    @staticmethod
    def delete_user(user):
        """Deletes the specified user.
        :param user: The user to delete
        """
        # self.delete(user)
