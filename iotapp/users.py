from functools import wraps

from flask import render_template, request, redirect, url_for, flash, abort, jsonify
from flask_wtf import Form
from flask_login import LoginManager, UserMixin, login_user, \
    current_user, logout_user
from wtforms import StringField, PasswordField, SubmitField  
from wtforms.validators import InputRequired
from passlib.hash import bcrypt_sha256
from itsdangerous import TimestampSigner, SignatureExpired, BadTimeSignature

from iotapp import app, applogger, db
from iotapp.models import UserDatastore

login_manager = LoginManager(app)
login_manager.login_view = "login"
login_manager.login_message = "Please log in to access this page."
login_manager.login_message_category = "info"
login_manager.session_protection = "strong"

access_levels = {'root': 0,
                'admin': 1,
                'full': 2,
                'view-only': 3,
                'device': 4}

user_roles = {level: role for role, level in access_levels.items() if role != 'root'}

class User(UserMixin):
    def __init__(self, user_tuple):
        self.user_id = str(user_tuple[0])
        self.password = user_tuple[1]
        self.access_level = user_tuple[2]
    
    def get_id(self):
        return str(self.user_id)

    @staticmethod
    def get_role(access_level):
        return user_roles[access_level]

    @staticmethod
    def load_user(user_id, password):
        """Loads the user if the user_id and password is valid.
        Otherwise returns None.
        """
        user_tuple = UserDatastore.find_user(user_id)
        if not user_tuple:
            return None

        user = User(user_tuple)
        if not UserDatastore.validate_password(password, user.password):
            return None
        
        return user

    @staticmethod
    def create_user(user_id, password, role='view-only'):
        """Validates and creates user. Unsuccessful operations will return a status code.
        1: Duplicate user_id
        2: Attempting to create root account
        3: Specified role does not exist
        """
        if role == 'root':
            return 2
        if role not in access_levels.keys():
            return 3

        user_access_level = access_levels.get(role)
        user = User((user_id, password, user_access_level))
        
        return UserDatastore.create_user(user)
    
@login_manager.user_loader
def load_user(user_id):
    return User(UserDatastore.find_user(user_id))

@login_manager.request_loader
def load_user_request(request):
    applogger.debug('Loading user with request')
    
    # first, try to login using the api_key url arg
    token = request.args.get('token')

    if token is not None:
        applogger.debug('Attempting login with token')
        s = TimestampSigner(app.config['SECRET_KEY'])
        try:
            user_id = s.unsign(token, max_age=100000).decode()
        except SignatureExpired:
            applogger.debug('Expired token')
            return None
        except BadTimeSignature:
            applogger.debug('Invalid token')
            return None


        user = User(UserDatastore.find_user(user_id))
        if user is None:
            return None
    
        return user
    
    def load_from_credentials(credentials):
        try:
            user_id = credentials.get('user_id')
            password = credentials.get('password')
        except AttributeError:
            applogger.debug('invalid credentials format')
            return None
      
        user = User(UserDatastore.authenticate_user(user_id, password))
        if user is None:
            return None
        
        applogger.debug('Loaded user with id %s', repr(user_id))

        return user

    if request.method == 'POST':
        try:
            data = request.get_json(force=True)
            credentials = data.get('credentials')
        except AttributeError:
            applogger.debug('Wrong credentials format')
            return
        else:
            return load_from_credentials(credentials)
    

def login_required(protected_methods=None):
    def wrapper(func):    
        
        @wraps(func)
        def decorated_view(*args, **kwargs):
            if login_manager._login_disabled:
                return func(*args, **kwargs)
            elif not current_user.is_authenticated:
                return login_manager.unauthorized()
            elif protected_methods is None:
                return func(*args, **kwargs)
            elif request.method in protected_methods:
                if current_user.access_level > protected_methods[request.method]:
                    return abort(403)
            return func(*args, **kwargs)

        return decorated_view
    
    return wrapper


def create_user(user_id, password, role='view-only'):
    """Validates and creates user. Unsuccessful operations will return a status code.
    1: Duplicate user_id
    2: Attempting to create root account
    3: Specified role does not exist
    """
    if role == 'root':
        return 2
    if role not in access_levels.keys():
        return 3

    user_access_level = access_levels.get(role)
    
    hashed_pwd = bcrypt_sha256.encrypt(password)
    args = {'user_id': user_id, 
            'password': hashed_pwd,
            'access_level':user_access_level}
    try:
        db.execute('psql_root', 'add_new_user', args, tuple(args.keys()))
    except ValueError:
        applogger.warning('Failed to add user with id "%s"', user_id)
        return 1

class LoginForm(Form):
    user_id = StringField('User ID', [InputRequired()])
    password = PasswordField('Password', [InputRequired()])
    submit = SubmitField('Login')

@app.route("/login", methods=['GET'])
def login():
    login_form = LoginForm()
    return render_template('login.html', login_form=login_form)
        
@app.route("/login", methods=['POST'])
def login_post():
    login_form = LoginForm()
    if not login_form.validate_on_submit():
        applogger.info('Login Failed with user id %s', login_form.user_id.data)
        flash('Invalid form submission', 'warning')
        
        return redirect(url_for('login'))

    user_id = login_form.user_id.data
    user = User.load_user(user_id, login_form.password.data)

    if not user:
        # login failed
        applogger.info('Login Failed with user id %s', user_id)
        flash('Invalid user id or password', 'warning')
        
        return redirect(url_for('login'))
    
    # Login and validate the user.
    # user should be an instance of your `User` class
    login_user(user)
    applogger.info('Login successful with id %s', user_id)
    flash('Welcome %s' % user.user_id, 'success')
    next_page = request.args.get('next')
    return redirect(next_page or url_for('index'))

@app.route("/login/token", methods=['POST'])
def login_post_api():
    data = request.get_json(force=True)
    if data is None:
        abort(400, 'Requires Json')

    try:
        user_id = data.get("user_id")
        password = data.get("password")
    except AttributeError:
        abort(400, 'Requires dict')

    if user_id is None or password is None:
        abort(400, 'Requires password and user_id field')
    user = User.load_user(user_id, password)
    if user is None:
        abort(403, 'Invalid PasswordField')

    print(data)
    s = TimestampSigner(app.config['SECRET_KEY'])
    rv = {'token': s.sign(user_id).decode()}

    return jsonify(**rv)


@app.route("/logout")
@login_required()
def logout():
    logout_user()
    return redirect('/login')