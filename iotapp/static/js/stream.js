//console.log for debugging purposes

$(function () {
    $(document).ready(function () {
        Highcharts.setOptions({
            global: {
                useUTC: false
            },
			
			chart: {
				type: 'line',
				animation: Highcharts.svg, // don't animate in old IE
                marginRight: 10,
                events: {
                },
			},
			
			xAxis: {
				title: {
					text: 'Time'
				},
                type: 'datetime',
                tickPixelInterval: 150
            },
			
			tooltip: {
                formatter: function () {
                    return '<b>' + this.series.name + '</b><br/>' +
                        Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) + '<br/>' +
                        Highcharts.numberFormat(this.y, 2);
                }
            },
        });
        var loaded_data = get_data();
        var stream = true;
		var data_temperature = [];
		var data_heartrate = [];
		var data_spo2 = [];
		$.each(loaded_data.res, function(i, list){
			data_temperature.push([list[0],list[1]]);
			data_heartrate.push([list[0],list[2]]);
			data_spo2.push([list[0],list[3]])
		});
		
        var chart_temperature = new Highcharts.Chart({
            chart: {
                renderTo: 'container'
            },
            title: {
                text: 'Temperature(°C)'
            },
            
            yAxis: {
                title: {
                    text: 'Temperature (°C)'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },

            legend: {
                enabled: false
            },
            exporting: {
                enabled: true
            },
            series: [{
                name: 'Temperature',
                data: data_temperature,
                lineWidth: 1.5,
                marker: {
                    enabled: false,
                },
            }]					

		});			
		
		var chart_heartrate = new Highcharts.Chart({
            chart: {

                renderTo: 'container2'
            },
            title: {
                text: 'Heart Rate'
            },

            yAxis: {
                title: {
                    text: 'Heart Rate (BPM)'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            
            legend: {
                enabled: false
            },
            exporting: {
                enabled: true
            },
            series: [{
                name: 'Heart Rate',
                data: data_heartrate,
                lineWidth: 1.5,
                marker: {
                    enabled: false,
                },
            }]					

		});
		
		var chart_spo2 = new Highcharts.Chart({
            chart: {

                renderTo: 'container3'
            },
            title: {
                text: 'Blood Pressure'
            },

            yAxis: {
                title: {
                    text: 'Blood Pressure (mmHg)'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            
            legend: {
                enabled: false
            },
            exporting: {
                enabled: true
            },
            series: [{
                name: 'SPO2',
                data: data_spo2,
                lineWidth: 1.5,
                marker: {
                    enabled: false,
                },
            }]					

		});
		
		/* Export HighCharts to pdf */
		(function (H) {
			H.Chart.prototype.createCanvas = function (divId) {
				var svg = this.getSVG(),
					width = parseInt(svg.match(/width="([0-9]+)"/)[1]),
					height = parseInt(svg.match(/height="([0-9]+)"/)[1]),
					canvas = document.createElement('canvas');

				canvas.setAttribute('width', width);
				canvas.setAttribute('height', height);

				if (canvas.getContext && canvas.getContext('2d')) {

					canvg(canvas, svg);

					return canvas.toDataURL("image/jpeg");

				} 
				else {
					alert("Your browser doesn't support this feature, please use a modern browser");
					return false;
				}
			}
		}(Highcharts));
		
		$('#export_button').click(function () {
			var doc = new jsPDF();

			// chart height defined here so each chart can be palced
			// in a different position
			var chartHeight = 50;

			// All units are in the set measurement for the document
			// This can be changed to "pt" (points), "mm" (Default), "cm", "in"
			doc.setFontSize(20);
			doc.setFontType("bold");
			doc.text(85, 25, "Vital Signs");
			doc.setLineWidth(0.5);
			doc.line(20, 35, 200, 35);
			
			//Add Patient Details & Time Generated
			var timeGenerated = new Date();
			doc.setFontSize(10);
			doc.setFontType("normal");
			doc.text(140, 33, "Generated at: " + timeGenerated.toLocaleString());
			
			
			//Add images to pdf
			doc.addImage(chart_temperature.createCanvas(), 'JPEG', 30, (0 * chartHeight) + 50, 150, chartHeight);
			doc.addImage(chart_heartrate.createCanvas(), 'JPEG', 30, (1 * chartHeight) + 50, 150, chartHeight);
			doc.addImage(chart_spo2.createCanvas(), 'JPEG', 30, (2 * chartHeight) + 50, 150, chartHeight);
			
			//save with name
			doc.save('VitalSignsChart.pdf');
		});
		
		//Refresh Data
		
		$('#refresh').click(function() {
            console.log('triggered')
            if (stream) {
                stream = false;
                document.getElementById("refresh").value = "Normal"
            } else {
                stream = true;
                document.getElementById("refresh").value = "Paused";
                data_temperature = [];
				data_heartrate = [];
				data_spo2 = [];
				$.each(loaded_data.res, function(i, list){
					data_temperature.push([list[0],list[1]]);
					data_heartrate.push([list[0],list[2]]);
					data_spo2.push([list[0],list[3]])
						});
						
				chart_temperature.series[0].setData(data_temperature);
				chart_heartrate.series[0].setData(data_heartrate);
				chart_spo2.series[0].setData(data_spo2);
            }
		});
        $(function worker() {
            $.ajax({
                url: '/api-query/get_records',
                data: loaded_data.args,
                success: function(data) {
                    if (stream) {
                        loaded_data.res = data.res;
						data_temperature = [];
						data_heartrate = [];
						data_spo2 = [];
						$.each(loaded_data.res, function(i, list){
						data_temperature.push([list[0],list[1]]);
						data_heartrate.push([list[0],list[2]]);
						data_spo2.push([list[0],list[3]])
						});
						
						chart_temperature.series[0].setData(data_temperature);
						chart_heartrate.series[0].setData(data_heartrate);
						chart_spo2.series[0].setData(data_spo2);

                        console.log('updated')
                    }
                    else {
                        loaded_data.res = data.res
                    }
                },
                complete: function() {
                setTimeout(worker, 5000);
                console.log('Done')
                }
            });
        })
    });
});