import datetime as dt

from flask import jsonify, request, abort

from iotapp import app, applogger, db, cache
from iotapp.users import login_required
from iotapp.models import RecordsDatastore

def return_message(status_code, message, data=None):
    rv = {}
    rv['data'] = data
    rv['message'] = message
    rv = jsonify(**rv)
    
    return (rv, status_code)

def prepare_records(data):
    data['res'] = [(record_time.timestamp() * 1000, temperature, heart_rate, spo2) for (record_time,temperature, heart_rate, spo2) in data['res']]
    return jsonify(**data)

@app.route('/api-query/get_records', methods=['GET'])
@login_required()
def get_records():
    if request.args.get('patient_IC') and request.args.get('device_tag'):
        return 'not supported'
    elif request.args.get('patient_IC'):
        args = {'patient_IC': request.args['patient_IC'],
                'record_time': dt.datetime.utcnow() - dt.timedelta(minutes=100)}
        data = db.execute('psql_root', 'select_patient_records', args)
        return prepare_records(data)
    elif request.args.get('device_tag'):
        args = {'device_tag': request.args['device_tag'],
                'record_time': dt.datetime.utcnow() - dt.timedelta(minutes=100)}
        data = db.execute('psql_root', 'select_device_records', args)
        return prepare_records(data)
    return return_message(400, 'Requires patient_IC or device_tag')


@app.route('/api-query/cache_assignments', methods=['GET'])
@login_required()
def get_cache():
    return jsonify(**cache)

def verify_records_data(data):
    """Check that data is in correct format to be inserted"""
    record_time = data['record_time']
    data['record_time'] = dt.datetime.fromtimestamp(record_time)
    return data
    

@app.route('/api-post/add_records', methods=['POST'])
@login_required()
def add_records():
    payload = request.get_json(force=True)
    if payload is None:
        return return_message(400, 'Json required')
    
    try:
        data = payload['data']
    except TypeError:
        return return_message(400, 'No data')

    try:
        data = verify_records_data(data)
    except ValueError:
        return return_message(400, 'Incorrect data format')

    applogger.debug("Received Data")
    
    RecordsDatastore.insert_record(data)

    return return_message(200, 'Success')
