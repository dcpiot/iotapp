import datetime as dt
import json

from flask import render_template, request, redirect, flash, url_for, abort, send_file
from flask_wtf import Form
from wtforms import StringField, SubmitField, SelectField, HiddenField
from wtforms.validators import InputRequired

from iotapp import app, applogger, db, cache, ppg_cache
from iotapp.users import user_roles, User, login_required
from iotapp.models import RecordsDatastore


@app.route('/', methods=['GET'])
@login_required({'GET': 3})
def index():
    numPatients = db.execute('psql_root', 'count_patients')
    numDevices = db.execute('psql_root', 'count_devices')
    if request.method == 'GET':
        return render_template('index.html',
                numPatients=numPatients,
                numDevices=numDevices)
    
@app.route('/test', methods=['POST', 'GET'])
@login_required({'GET': 2, 'POST': 2})
def showTestPage():
    import random
    if request.method == 'GET':
        random_data = {'temperature': 36.9 + random.gauss(0, 1),
            'heart_rate': 80 + int(random.gauss(0, 15)),
            'spo2': 50 + random.gauss(0, 15)}
        return render_template('test.html', defaults=random_data)
    elif request.method == 'POST':
        device_tag = request.form['device_tag']
        temperature = request.form['temperature']
        heart_rate = request.form['heart_rate']
        spo2 = request.form['spo2']
        sample_data = {'device_id':device_tag, 
                'record_time':dt.datetime.utcnow(), 
                'temperature': float(temperature),
                'heart_rate': int(heart_rate),
                'spo2': float(spo2),
                'ppg': None}
        if device_tag:
            RecordsDatastore.insert_test_record(sample_data)
        return redirect('/test')

@app.route('/view', methods=['GET'])
@login_required({'GET': 3,})
def show_graph_view():
    query_choice = request.args.get('query_choice')
    if query_choice is None:
        return render_template('view.html')
    if query_choice == 'device_tag':
        args = {'device_tag': request.args.get('query_value'),
            'record_time':dt.datetime.utcnow()}
        data = db.execute('psql_root', 'select_device_records', args)
    elif query_choice == 'patient_IC':
        args = {'patient_IC': request.args.get('query_value'),
            'record_time':dt.datetime.utcnow()}
        data = db.execute('psql_root', 'select_patient_records', args)  
    if data['res']:
        args.pop('record_time')
        data['res'] = [(record_time.timestamp() * 1000, temperature, heart_rate, spo2) for (record_time,temperature, heart_rate, spo2) in data['res']]
        data['args'] = args
        return render_template('view_query.html', data=json.dumps(data))
    else:
        flash('No results from query', 'info')
        return redirect(url_for('show_graph_view'))

@app.route('/view/ppg/<ppg_key>/<time_key>', methods=['GET'])
def wave_plotter(ppg_key, time_key):
    """
    ppg data format : dict with keys 'record_times' and 'values'
    each containing an array of values, and sorted by record time
    """
    applogger.info('plotting image for ppg_key %r and time_key %r', ppg_key, time_key)
    import matplotlib
    matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    fig = plt.figure()

    ppg = ppg_cache.get(ppg_key)

    if ppg is None:
        return None

    ppg['records_times'] = \
            [dt.datetime.fromtimestamp((record_time)) for record_time in ppg['record_times']]

    ppg['records_times'] = matplotlib.dates.date2num(ppg['records_times'])


    ax = fig.add_subplot(111)
    ax.plot_date(ppg['records_times'], ppg['values'], fmt='-')

    from tempfile import NamedTemporaryFile
    imgdata = NamedTemporaryFile(mode='w+b', suffix='jpg')
    fig.savefig(imgdata, format='png')
    imgdata.seek(0)  # rewind the data
    return send_file(imgdata, mimetype='image/png')

@app.route('/view/ppg', methods=['GET'])
@login_required({'GET': 3,})
def show_waveform():
    query_choice = request.args.get('query_choice')
    if not query_choice:
        flash('No parameters specified')
        return render_template('view_wave.html')
    query_value = request.args.get('query_value')
    res = RecordsDatastore.obtain_ppg(query_choice, query_value)
    if res:
        time_key = int(dt.datetime.utcnow().timestamp() // 3)
        ppg_key = query_choice + query_value
        ppg_cache[ppg_key] = res
        applogger.info('obtained ppg and stored in key %r', ppg_key)
        return render_template('view_wave.html', ppg_key=ppg_key, time_key=time_key)
    else:
        applogger.info('no ppg results')
        flash('No ppg_results', 'info')
        return render_template('view_wave.html')


class CreatePatientForm(Form):
    patient_ic = StringField('Patient IC', validators=[InputRequired()])
    name = StringField('Patient Name', validators=[InputRequired()])

@app.route('/patients', methods=['GET', 'POST'])
@login_required({'POST': 3, 'GET': 3})
def manage_patients():

    form = CreatePatientForm()
    table = db.execute('psql_root', 'list_patients')
    if request.method == 'GET': 
        return render_template('patients.html', form=form, table=table)
    elif request.method == 'POST':
        if form.validate_on_submit():
            args = {'name': form.name.data,
                    'IC': form.patient_ic.data}
            try:
                db.execute('psql_root', 'add_new_patient', args)
            except ValueError:
                flash('Patient already exists', 'info')
                return redirect(url_for('manage_patients'))
            flash('Successfully added patient', 'info')
            return redirect(url_for('manage_patients'))
        else:
            applogger.debug('Invalid Form received')
            abort(400)


class CreateUserForm(Form):
    username = StringField('Username', validators=[InputRequired()])
    user_role = SelectField('Username',
        choices=[(role, role) for role in user_roles.values()],
        validators=[InputRequired()])
    password = StringField('Password', validators=[InputRequired()])
    submit = SubmitField('Create')

class DeleteUserForm(Form):
    username = HiddenField('Username', id="delete-form-username", validators=[InputRequired()])
    submit = SubmitField('Delete')

@app.route('/administrator', methods=['GET', 'POST'])
@login_required({'POST': 1, 'GET': 1})
def administrator():
    create_form = CreateUserForm()
    delete_form = DeleteUserForm()
    numUsers = db.execute('psql_root', 'count_users')
    users = db.execute('psql_root', 'list_users')['res']
    table = [(user[0], user[1], User.get_role(user[2])) for user in users]
    if request.method == 'GET':
        return render_template('administrator.html', 
        numUsers=numUsers,
        table=table,
        create_form=create_form,
        delete_form=delete_form)

    elif request.method == 'POST':
        if request.form['submit'] == 'create' and create_form.validate_on_submit():
            applogger.debug('Received valid {} form'.format(request.form['submit']))
            args = {'user_id': create_form.username.data, 
                    'password': create_form.password.data,
                    'role': create_form.user_role.data}
            status = User.create_user(**args)
            if status is None:
                flash('User created with role {}'.format(create_form.user_role.data), 'info')
                return redirect('/administrator')
            elif status == 1:
                flash('Userid already exists', 'warning')
                return redirect(url_for('administrator'))
            else:
                applogger.warning('Create user failed with status %s', status)
                abort(400)

        elif request.form['submit'] == 'delete' and delete_form.validate_on_submit():
            applogger.debug('Received valid %s form', request.form['submit'])
            args = {'user_id': delete_form.username.data}
            res = db.execute('psql_root', 'delete_user', args)
            if res['rowcount'] == 1:
                flash('User {} deleted'.format(delete_form.username.data), 'success')
                return redirect(url_for('administrator'))
            elif res['rowcount'] == 0:
                flash('Unable to delete user {}'.format(delete_form.username.data), 'error')
                return redirect(url_for('administrator'))
        else:
            applogger.warning('{} form invalidated'.format(request.form['submit']))
            abort(400)

class CreateDeviceForm(Form):
    device_tag = StringField('Device Tag', validators=[InputRequired()])
    description = StringField('Description')
    submit = SubmitField('Create')

class AssignDeviceForm(Form):
    device_tag = StringField('Device Tag', validators=[InputRequired()])
    patient_ic = StringField('Patient IC', validators=[InputRequired()])
    submit = SubmitField('Assign')

class UnassignDeviceForm(Form):
    device_tag = StringField('Device Tag', validators=[InputRequired()])
    submit = SubmitField('Unassign')

@app.route('/devices', methods=['GET', 'POST'])
@login_required({'POST': 2, 'GET': 3})
def manage_devices():
    def get_time_dif(record_time):
        if record_time is None:
            return None
        record_time = dt.datetime.replace(record_time, tzinfo=None)
        secs_diff = (dt.datetime.utcnow() - record_time).seconds
        return dt.timedelta(seconds = secs_diff)

    create_form = CreateDeviceForm()
    assign_form = AssignDeviceForm()
    unassign_form = UnassignDeviceForm()
    
    table = db.execute('psql_root', 'list_devices')

    applogger.warning('%r', table)
    table['res'] = [(device[0], device[1], device[2], get_time_dif(device[3]))\
            for device in table['res']]
    if request.method == 'GET':
        return render_template('devices.html', 
            create_form=create_form, 
            assign_form=assign_form,
            unassign_form=unassign_form,
            table=table)
    elif request.method == 'POST':
        if request.form['submit'] == 'create' and create_form.validate_on_submit() :
            applogger.debug('Received valid {} form'.format(request.form['submit']))
            args = {'tag': create_form.device_tag.data, 
                    'description': create_form.description.data}
            try:
                db.execute('psql_root', 'add_new_device', args)
            except ValueError:
                applogger.warn('Returned exception')
                flash('Device already exists', 'info')
                return redirect(url_for('manage_devices'))
            return redirect('/devices')
        elif request.form['submit'] == 'assign' and assign_form.validate_on_submit():
            applogger.debug('Received valid %s form', request.form['submit'])
            args = {'tag': assign_form.device_tag.data, 
                    'IC': assign_form.patient_ic.data}
            try:
                update = db.execute('psql_root', 'assign_device', args)
                print(update)
            except ValueError:
                applogger.warning('Returned exception')
                return 'incorrect input'
            if not update['res']:
                applogger.warn('Returned exception')
                flash('Already assigned or patient does not exist', 'info')
                return redirect(url_for('manage_devices'))
            assigned_id = update['res'][0][0]
            cache[args['tag']] = assigned_id
            return redirect('/devices')
        elif request.form['submit'] == 'unassign' and unassign_form.validate_on_submit():
            applogger.debug('Received valid {} form'.format(request.form['submit']))
            args = {'tag': unassign_form.device_tag.data}
            update = db.execute('psql_root', 'unassign_device', args)
            if update['rowcount'] == 0:
                return 'device not assigned or does not exist'
            cache[args['tag']] = None
            return redirect('/devices')
        else:
            applogger.warning('{} form invalidated'.format(request.form['submit']))
            abort(400)
            