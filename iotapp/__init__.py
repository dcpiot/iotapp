import os

from flask import Flask

from shared.logger import create_logger
from shared.config import AppConfig
from database import db


applogger = create_logger('app')
applogger.debug('---Initiated---')

basedir = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__)
app.config.from_object(AppConfig)
app.permanent_session_lifetime = 3600

def init_cache():
    cache = {}
    assignments = db.execute('psql_root', 'list_device_assignments')
    for assignment in assignments['res']:
        cache[assignment[0]] = assignment[1]
    return cache


cache = init_cache()

ppg_cache = {}

from iotapp import api, views, users
