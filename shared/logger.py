import os
import sys
import logging

from shared.config import ENVIRONMENT

logfolder =  os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir, 'logs'))
if not os.path.exists(logfolder):
    os.mkdir(logfolder)

def setrootlogger():
    rootlogger = logging.getLogger()
    rootlogger.setLevel(logging.DEBUG)
    ch = logging.StreamHandler(sys.stdout)
    if ENVIRONMENT == 'localhost':
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        ch.setFormatter(formatter)
        fname = 'log_{}.log'
        handler_info = logging.FileHandler(os.path.join(logfolder, fname.format('info')))
        handler_info.setFormatter(formatter)
        handler_info.setLevel(logging.INFO)
        rootlogger.addHandler(handler_info)
    elif ENVIRONMENT == 'heroku':
        formatter = logging.Formatter('%(name)s - %(levelname)s - %(message)s')
        ch.setFormatter(formatter)
    ch.setLevel(logging.DEBUG)
    rootlogger.addHandler(ch)
        

def create_logger(name, logfolder=logfolder):
    fname= name + 'logger_{}.log'
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)
    if ENVIRONMENT == 'localhost':
        formatter = logging.Formatter(
            '%(asctime)s %(name)-12s %(levelname)-8s %(message)s')
        handler_debug = logging.FileHandler(os.path.join(logfolder, fname.format('debug')))
        handler_debug.setFormatter(formatter)
        handler_debug.setLevel(logging.DEBUG)
        handler_info = logging.FileHandler(os.path.join(logfolder, fname.format('info')))
        handler_info.setFormatter(formatter)
        handler_info.setLevel(logging.INFO)
        logger.addHandler(handler_info)
        logger.addHandler(handler_debug)

    return logger

setrootlogger()