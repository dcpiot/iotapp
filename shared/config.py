from os import environ

DATABASE_URI = environ.get('DATABASE_URL')
ENVIRONMENT = environ.get('ENVIRONMENT')

class AppConfig(object):
    DEBUG = False
    SECRET_KEY = environ.get('SECRET_KEY')
    PERMANENT_SESSION_LIFETIME = 3600 * 24
    MAX_CONTENT_LENGTH = 1024 * 1024 * 2

try:
    from testConfig import *
except ImportError:
    pass