"""
Manage user accounts here

Usage:
Create account: -c username password
"""


import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), os.pardir))

from collections import namedtuple

from passlib.hash import bcrypt_sha256

from shared.logger import create_logger
from database.dbconnector import Dbconnector

_queries = {
    'add_user': Dbconnector._query_tuple("""INSERT INTO users
        (user_id, password, access_level) VALUES (%s, %s, 0)""", ('user_id', 'password')),
    'add_device': Dbconnector._query_tuple("""INSERT INTO users
        (user_id, password, access_level) VALUES (%s, %s, 4)""", ('user_id', 'password')),
        }

database = 'psql_root'

def add_user(argv, tag, proc):
    
    user_id = argv[argv.index(tag) + 1]
    user_pwd = argv[argv.index(tag) + 2]
    hashed_pwd = bcrypt_sha256.encrypt(user_pwd) 
    args = {'user_id': user_id, 'password': hashed_pwd}
    db.execute(database, proc, args)


if __name__ == "__main__":
    db = Dbconnector(create_logger('db'))
    db._queries = _queries
    
    argv = sys.argv[1:]
    if '-c' in argv:
        add_user(argv, '-c', 'add_user')
    if '-c-d' in argv:
        add_user(argv, '-c-d', 'add_device')
        