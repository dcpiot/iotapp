import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), os.pardir))

from shared.logger import create_logger
from database.dbconnector import Dbconnector

_queries = {
        'drop_tables': Dbconnector._query_tuple("""DROP SCHEMA public CASCADE;
            CREATE SCHEMA public;""", None),
            }

if __name__ == "__main__":
    db = Dbconnector(create_logger('db'))
    db._queries = _queries
    
    args = sys.argv[1:]
    if '-d' in args:
        database = args[args.index('-d') + 1]
        db.execute(database, 'drop_tables')

    db.initiate('psql_root')
        