import os
import sys
import time
import datetime as dt
import random

sys.path.append(os.path.join(os.path.dirname(__file__), os.pardir))

from database.dbconnector import Dbconnector
from shared.logger import create_logger

"""
Here we inject data directly into the database, so we can see how it looks like in HighCharts.
And also check if the streaming works.
"""

test_db = 'psql_root'
test_device_id = 2

db = Dbconnector(create_logger('dbtest'))

def simulate():
    base = dt.datetime.utcnow() - dt.timedelta(minutes=100)
    tuple_args = [(test_device_id, base + dt.timedelta(seconds=5 * x), random.random() * 30, x, x) for x in range(1200)]

    db._execute_bulk(test_db, 'insert_record', tuple_args)
    x = 0
    while True:
        time.sleep(5)
        db._execute_bulk(test_db, 'insert_record', [(test_device_id, dt.datetime.utcnow(), x, x, x)])
        x += 5

try:
    simulate()
except KeyboardInterrupt:
    print('\n\n---DELETING DATA AND EXITING---\n\n')
    db.execute(test_db, 'drop_tables')
    db.initiate(test_db)
    raise KeyboardInterrupt