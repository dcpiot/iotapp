#!/usr/bin/python
import json
import os
import time
from collections import namedtuple

import psycopg2
import psycopg2.extras
from psycopg2 import IntegrityError, ProgrammingError
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

from shared.config import DATABASE_URI

QueryTuple = namedtuple('query', 'sql_statement, input_args')

class Dbconnector():
    _query_tuple = QueryTuple
    _queries = {
        'select_device_records': _query_tuple("""SELECT raw_data.record_time, 
            raw_data.temperature, raw_data.heart_rate, raw_data.spo2
            FROM raw_data INNER JOIN devices ON raw_data.device_id = devices.id 
            WHERE devices.tag = %s ORDER BY raw_data.record_time ASC LIMIT 200""",
            ('device_tag',)),
        'select_patient_records': _query_tuple("""SELECT 
            raw_data.record_time, raw_data.temperature, raw_data.heart_rate, raw_data.spo2
            FROM raw_data INNER JOIN patients ON raw_data.patient_id = patients.id WHERE
            patients.IC = %s ORDER BY raw_data.record_time ASC LIMIT 200""",
            ('patient_IC',)),
        'add_new_device': _query_tuple("""INSERT INTO devices (tag, description)
            VALUES (%s, %s);""", ('tag', 'description')),
        'add_new_patient': _query_tuple("""INSERT INTO patients (IC, name)
            VALUES (%s, %s);""", ('IC', 'name')),
        'list_devices': _query_tuple("""SELECT devices.tag, devices.description, patients.IC, devices.record_time
            FROM devices LEFT JOIN patients ON devices.patient_id = patients.id 
            ORDER BY devices.tag ASC""", None),
        'list_patients': _query_tuple("""SELECT IC, name FROM patients ORDER BY IC ASC""", None),
        'assign_device': _query_tuple("""UPDATE devices SET patient_id = patients.id
            FROM patients WHERE patients.IC = %s AND devices.tag = %s 
              AND patients.id IS DISTINCT FROM devices.patient_id
            RETURNING devices.patient_id""", ('IC', 'tag')),
        'list_device_assignments': _query_tuple("""SELECT tag, patient_id FROM devices""",
            None),
        'count_patients': _query_tuple("""SELECT COUNT(*) FROM patients""", None),
        'count_devices': _query_tuple("""SELECT COUNT(*) FROM devices""", None),
        'unassign_device': _query_tuple("""UPDATE devices SET patient_id = NULL
            FROM patients WHERE devices.tag = %s AND devices.patient_id IS NOT NULL""", ('tag',)),
        'load_user': _query_tuple("""SELECT user_id, password, access_level FROM users 
            WHERE user_id = %s""", ('user_id',)),
        'count_users': _query_tuple("""SELECT COUNT(*) FROM users
            WHERE access_level <> 0""", None),
        'list_users': _query_tuple("""SELECT id, user_id, access_level FROM users
            WHERE access_level <> 0 ORDER BY access_level ASC, user_id ASC""", None),
        'add_new_user': _query_tuple("""INSERT INTO users 
            (user_id, password, access_level) VALUES (%s, %s, %s);""",
            ('user_id', 'password', 'access_level')),
        'delete_user': _query_tuple("""DELETE FROM users WHERE user_id = %s""", ('user_id', ))
        }
    _databases = {'psql_root': DATABASE_URI}

    def __init__(self, logger):
        self.logger = logger
        self.cursor = {database[0]: self.connect(database) for database in self._databases.items()}	

    def _execute_query(self, database, query_tuple, args=None, log_args=None):
        
        if log_args is None:
            log_args = args
        self.logger.debug("Executing '{}' on database '{}'\n    with values '{}'".format(query_tuple.sql_statement, database, log_args))
        
        # verify and extract input args
        # an exception will be thrown if an argument is not present
        if query_tuple.input_args:	
            args = tuple(args[input_arg] for input_arg in query_tuple.input_args)
        
        # run sql statement against database
        starttime = time.time()
        try:
            self.cursor[database].execute(query_tuple.sql_statement, args)
        except IntegrityError as e:
            self.logger.debug(e)
            raise ValueError('Constraint Violated executing query')
        self.logger.debug("Executed in {}s".format(time.time() - starttime))
        
        # get results
        retval = {}
        retval['rowcount'] = self.cursor[database].rowcount
        self.logger.debug("Affected rowcount: {}".format(retval['rowcount']))
        try:
            retval['res'] = self.cursor[database].fetchall()
        except ProgrammingError as e:
            self.logger.debug(e)
        else:
            retval['col_names'] = [desc[0] for desc in self.cursor[database].description]
            self.logger.debug("Fetched columns: {} with {} rows".format(retval['col_names'], len(retval['res'])))
        
        return retval

    def execute(self, database, query, args=None, log_args=None):
        query_tuple = self._queries[query]
        return self._execute_query(database, query_tuple, args, log_args)
    
    def execute_query(self, database, query, args=None, log_args=None):
        print(query[0])
        print(query[1])
        query_tuple = self._query_tuple(*query)
        return self._execute_query(database, query_tuple, args, log_args)

    def _execute_bulk(self, database, query, args_list=None):
        """
        Use only for testing. Slow insertion. 
        Should not be required for normal operations.
        """
        query = self._queries[query]
        self.logger.debug("Execute query '{}' on database '{}'\n    with length '{}'"\
            .format(query.sql_statement, database, len(args_list)))
        starttime = time.time()
        self.cursor[database].executemany(query.sql_statement, args_list)
        self.logger.debug("Executed in {}s".format(time.time() - starttime))

    def connect(self, database):
        dbname, uri = database
        self.logger.debug("Connecting to database %s" % repr(dbname))
        # get a connection, if a connect cannot be made an exception will be raised here
        conn = psycopg2.connect(uri)
        conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
        self.logger.info('Connected to %s' % repr(dbname))
    
        # conn.cursor will return a cursor object, you can use this cursor to perform queries
        return conn.cursor()
    
    def initiate(self, database):
        for database in self._databases.items():
            with self.connect(database) as curser:
                schema_path = os.path.join(os.path.dirname(__file__), 'dbschema_%s.sql' % database[0])
                curser.execute(open(schema_path, "r").read())
                self.logger.debug('Database "%s" created' % database[0])


    