CREATE TABLE IF NOT EXISTS users (
    id SERIAL PRIMARY KEY,
    user_id VARCHAR UNIQUE NOT NULL,
    access_level INT NOT NULL,
    password VARCHAR NOT NULL
);

CREATE UNIQUE INDEX ON users (access_level) where access_level = 0;

CREATE TABLE IF NOT EXISTS patients (  
    id SERIAL PRIMARY KEY,
    IC VARCHAR UNIQUE NOT NULL,
    name VARCHAR NOT NULL
);

CREATE TABLE IF NOT EXISTS devices (  
    id SERIAL PRIMARY KEY NOT NULL,
    tag VARCHAR UNIQUE NOT NULL,
    description VARCHAR,
    record_time TIMESTAMP WITH TIME ZONE NOT NULL,
    status INT,
    patient_id INT REFERENCES patients (id) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS raw_data (
    id SERIAL PRIMARY KEY,
    device_id INT REFERENCES devices(id) ON DELETE RESTRICT ON UPDATE CASCADE NOT NULL ,
    patient_id INT REFERENCES patients (id) ON DELETE RESTRICT ON UPDATE CASCADE,
    record_time TIMESTAMP WITH TIME ZONE NOT NULL,
    temperature REAL,
	heart_rate INT,
	spo2 REAL,
    ppg TEXT,
    UNIQUE (device_id, record_time)
);

CREATE TABLE IF NOT EXISTS device_assignments (  
    id SERIAL PRIMARY KEY,
    patient_id INT REFERENCES patients (id) ON DELETE RESTRICT ON UPDATE CASCADE,
    device_id INT REFERENCES devices (id) ON DELETE RESTRICT ON UPDATE CASCADE NOT NULL,
    modified_time TIMESTAMPTZ NOT NULL
);


CREATE OR REPLACE FUNCTION log_device_assignments()
    RETURNS trigger AS $device_assignments$
    BEGIN
        INSERT INTO device_assignments (patient_id, device_id, modified_time)
        VALUES(NEW.patient_id, NEW.id, now() at time zone 'utc');
        RETURN NEW;
    END;
$device_assignments$ LANGUAGE plpgsql;

CREATE TRIGGER device_assignments
    AFTER INSERT OR UPDATE OF patient_id
    ON devices
    FOR EACH ROW
    EXECUTE PROCEDURE log_device_assignments();